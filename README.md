<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Se Necesita</a></li>
<li><a href="#sec-2">2. Estructura</a></li>
<li><a href="#sec-3">3. Meta</a></li>
</ul>
</div>
</div>


# Se Necesita<a id="sec-1" name="sec-1"></a>

Es necesario emacs y el [modo Org](http://orgmode.org/).

# Estructura<a id="sec-2" name="sec-2"></a>

Se separan por temas y no por número de ejercios porque con el tiempo pueden cambiar. Por cada tema, se crea un archivo Org similar [al template dado](TEMPLATE.org.tpl). 

# Meta<a id="sec-3" name="sec-3"></a>
